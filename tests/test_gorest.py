from utils.gorest_handler import GoRESTHandler
from faker import Faker

go_rest_handler = GoRESTHandler()


def test_create_user():
    user_data = {
        "name": "Tenali Ramakrishna",
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    body = go_rest_handler.create_user(user_data).json()
    assert "id" in body
    user_id = body["id"]
    body = go_rest_handler.get_user(user_id).json()
    assert body["email"] == user_data["email"]
    assert body["name"] == user_data["name"]
    assert body["gender"] == user_data["gender"]
    assert body["status"] == user_data["status"]


def test_update_user_status():
    updated_user_data = {
        "name": "Tenali Ramakrishna",
        "gender": "male",
        "email": Faker().email(),
        "status": "inactive"
    }
    body = go_rest_handler.create_user(updated_user_data).json()
    assert "id" in body
    user_id = body["id"]
    body = go_rest_handler.get_user(user_id).json()
    assert body["email"] == updated_user_data["email"]
    assert body["name"] == updated_user_data["name"]
    assert body["gender"] == updated_user_data["gender"]
    assert body["status"] == updated_user_data["status"]


def test_delete_user():
    deleted_user_data = {
        "name": "",
        "gender": "",
        "email": "",
        "status": ""
    }
    body = go_rest_handler.delete_user(deleted_user_data).json()
    assert body["message"] == "Resource not found"

